package com.moex.passport.api;

import android.text.TextUtils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Abstract HttpURLConnection factory.
 */
public abstract class Endpoint {

    protected final String TAG = getClass().getSimpleName();

    public final static int DEFAULT_TIMEOUT = 10000;

    private final URL mEndpoint;

    private String mUserAgent;

    private int mTimeout = DEFAULT_TIMEOUT;

    Endpoint(final URL endpoint) {
        mEndpoint = endpoint;
    }

    Endpoint(final URL endpoint, final String userAgent) {
        this(endpoint);
        mUserAgent = userAgent;
    }

    public final URL getEndpoint() {
        return mEndpoint;
    }

    public int getTimeout() {
        return mTimeout;
    }

    public Endpoint setTimeout(int timeout) {
        mTimeout = timeout;
        return this;
    }

    public String getUserAgent() {
        return mUserAgent;
    }

    public Endpoint setUserAgent(final String agent) {
        mUserAgent = agent;
        return this;
    }

    private HttpURLConnection open(String resource) throws IOException {
        final URL url = new URL(mEndpoint, resource);
        final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        if (!TextUtils.isEmpty(mUserAgent))
          connection.setRequestProperty(HttpUtils.UserAgentName, mUserAgent);
        if (mTimeout > 0) {
            connection.setConnectTimeout(mTimeout);
            connection.setReadTimeout(mTimeout);
        }
        connection.setUseCaches(false);
        return connection;
    }

    protected HttpURLConnection open(String method, String resource) throws IOException {
        final HttpURLConnection connection = open(resource);
        connection.setRequestMethod(method);
//        Log.d(TAG, method + " " + connection.getURL().toString());
        return connection;
    }

//    protected void log(String what, Map<String, List<String>> headers) {
//        Log.d(TAG, what + " ==========");
//        for (Map.Entry<String, List<String>> h: headers.entrySet())
//            Log.d(TAG, h.getKey() + ": " + h.getValue());
//    }

}
