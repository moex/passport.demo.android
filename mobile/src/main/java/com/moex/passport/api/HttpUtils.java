package com.moex.passport.api;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.nio.charset.Charset;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Created by ZaitsevY on 19.01.2015.
 */
public class HttpUtils {

    final static String UserAgentName = "User-Agent";

    /**
     * Generate a string suitable for a {@code "User-Agent"} HTTP
     * request header.
     *
     * @param context
     * @return A user-agent string made of Android application
     * name, application version and Android OS version.
     */
    public static String getDefaultUserAgent(final Context context) {
        final StringBuilder agent = new StringBuilder();
        final Context ac = context.getApplicationContext();
        final PackageManager pm = ac.getPackageManager();
        final ApplicationInfo ai = ac.getApplicationInfo();
        agent.append(ai.loadLabel(pm));
        try {
            final PackageInfo pi = pm.getPackageInfo(ac.getPackageName(), 0);
            agent.append(' ').append(pi.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            // swalow
        }
        agent.append(" (").append(Build.DISPLAY).append(')');
        return agent.toString();
    }

    /**
     * Disables any SSL certificate certificate validation.
     * Can be used in development or demo environments when
     * server or client certificates are self-signed or
     * are invalid by any other reason.
     *
     * Configures {@link javax.net.ssl.HttpsURLConnection} default SSL socket
     * factory with an {@link javax.net.ssl.X509TrustManager} allowing any
     * client or server certificate and with a {@link javax.net.ssl.HostnameVerifier}
     * allowing any host name.
     *
     * Make sure that you do not use certificate in production environment
     */
    public static void disableSSLCertificateChecking() {
        try {
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, new TrustManager[] {new TrustAnyMManager()}, null);
            HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new AllowAnyHostnameVerifier());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
    }

    static class AllowAnyHostnameVerifier implements HostnameVerifier {

        @Override
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    }

    static class TrustAnyMManager implements X509TrustManager {

        @Override
        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            // no implementation: always trust
        }

        @Override
        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            // no implementation: always trust
        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }

    }

    /**
     * Retrieves content charset name from {@code "Content-Type"} response header,
     * if the header is present and is similar to the pattern like {@code "application/json; charset=utf-8"}
     *
     * @param connection
     * @return
     */
    public static String getContentCharset(HttpURLConnection connection) {
        final String type = connection.getContentType();
        if (type == null)
            return Charset.defaultCharset().name();
        final int p = type.lastIndexOf(';');
        if (p < 0)
            return Charset.defaultCharset().name();
        final String what = type.substring(p+1).trim();
        if (what.startsWith("charset=")) {
            final String name = what.substring(8);
            if (Charset.isSupported(name)) return name;
        }
        return Charset.defaultCharset().name();
    }

    /**
     * Loads all the content of connection input (or error) stream into string.
     *
     * @param connection
     * @return
     * @throws IOException
     */
    public static String load(HttpURLConnection connection) throws IOException {
        InputStream stream;
        try {
            stream = connection.getInputStream();
        } catch (FileNotFoundException e) {
            stream = connection.getErrorStream();
        }
        try {
            if (stream == null)
                return null;
            else {
                final String charset = getContentCharset(connection);
                final int length = connection.getContentLength();
                if (length >= 0) {
                    final byte[] buffer = new byte[length];
                    int offset = 0;
                    while (offset < length)
                        offset += stream.read(buffer, offset, length - offset);
                    return new String(buffer, charset);
                } else {
                    final byte[] buffer = new byte[16384];
                    final ByteArrayOutputStream output = new ByteArrayOutputStream();
                    while (true) {
                        final int loaded = stream.read(buffer);
                        if (loaded < 0) break;
                        output.write(buffer, 0, loaded);
                    }
                    return output.toString(charset);
                }
            }
        } finally {
            if (stream != null)
                stream.close();
        }
    }

    /**
     * Check if <i>connection</i> response contains a <i>header</i> with a <i>value</i>.
     *
     * @param connection - a valid connection to be checked for response
     * @param header - header name
     * @param value - header value
     * @return
     * @throws IOException
     */
    public static boolean hasHeaderValue(HttpURLConnection connection, String header, String value) throws IOException {
        if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
            List<String> items = connection.getHeaderFields().get(header);
            if (items == null) return false;
            for (String item: items)
                if (value.equals(item))
                    return true;
        }
        return false;

    }

    private HttpUtils() {}

}
