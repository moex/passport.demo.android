package com.moex.passport.api;

import android.text.TextUtils;

import java.io.IOException;
import java.net.HttpURLConnection;

/**
 * Created by ZaitsevY on 19.01.2015.
 */
public class HttpException extends IOException {

    private static final long serialVersionUID = 382509861768206379L;

    public final int code;

    private final String content;

    public HttpException(int code) {
        this.code = code;
        this.content = null;
    }

    public HttpException(int code, String message) {
        super(message);
        this.code = code;
        this.content = null;
    }

    public HttpException(int code, String message, String content) {
        super(message);
        this.code = code;
        this.content = content;
    }

    public HttpException(int code, Throwable cause) {
        super(cause);
        this.code = code;
        this.content = null;
    }

    public HttpException(int code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
        this.content = null;
    }

    public String getContent() {
        return content;
    }

    @Override
    public String toString() {
        final String text = getMessage();
        return TextUtils.isEmpty(text)
                ? Integer.toString(code) : Integer.toString(code) + " " + text;
    }

    public static HttpException build(HttpURLConnection connection) throws IOException, HttpException {
        String content;
        try {
            content = HttpUtils.load(connection);
        } catch (IOException e) {
            content = null;
        }
        return new HttpException(
                connection.getResponseCode(),
                connection.getResponseMessage(),
                content);
    }

}
