package com.moex.passport.api;

import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import java.io.IOException;
import java.net.CookieManager;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * MOEX Passport Java API.
 * @see <a href="https://passport.beta.moex.com/api/reference/passport"></a>
 *
 * Installs a {@link java.net.CookieManager} with default {@link java.net.CookieStore} and
 * {@link java.net.CookiePolicy}
 */
public class Passport extends Endpoint {

    public final static String PASSPORT_COOKIE_NAME = "MicexPassportCert";

    final static CookieStore sCookieStore;

    static {
        final CookieManager manager = new CookieManager();
        //sCookieStore = new CookieStoreWrapper(manager.getCookieStore());
        //CookieManager.setDefault(new CookieManager(sCookieStore, CookiePolicy.ACCEPT_ORIGINAL_SERVER));
        sCookieStore = manager.getCookieStore();
        CookieManager.setDefault(manager);
    }

    public Passport(final URL endpoint) {
        super(endpoint);
    }

    public Passport(final URL endpoint, final String userAgent) {
        super(endpoint, userAgent);
    }

    private URI getCookieURI() throws URISyntaxException {
        return new URI("http", getEndpoint().getHost(), null, null);
    }

    public String getSecurityToken() {
        try {
            final URI uri = getCookieURI();
            //Log.d(getClass().getSimpleName(), "gST uri=" + uri.toString());
            for (HttpCookie cookie : sCookieStore.get(uri)) {
                //Log.d(getClass().getSimpleName(), "gST cookie=" + cookie);
                if (PASSPORT_COOKIE_NAME.equals(cookie.getName()))
                    return cookie.getValue();
            }
        } catch (URISyntaxException e) {
            // swallow and fall through
        }
        return null;
    }

    public Passport setSecurityToken(String value) {
        try {
            final URI uri = getCookieURI();
            synchronized (sCookieStore) {
                if (TextUtils.isEmpty(value)) {
                    for (HttpCookie cookie : sCookieStore.get(uri)) {
                        if (PASSPORT_COOKIE_NAME.equals(cookie.getName()))
                            sCookieStore.remove(uri, cookie);
                    }
                } else
                    sCookieStore.add(uri, new HttpCookie(PASSPORT_COOKIE_NAME, value));
            }
        } catch (URISyntaxException e) {
            // swallow and fall through
        }
        return this;
    }

    public boolean isAuthenticated() {
        return !TextUtils.isEmpty(getSecurityToken());
    }

    /**
     * Authenticate username nad password against MOEX passport service using
     * basic authorization.
     *
     * @param username
     * @param password
     * @return MOEX passport certificate value in case of successful authentication.
     * @throws IOException in case of any failure.
     */
    public String authenticate(String username, String password) throws IOException {
        final HttpURLConnection connection = open("GET", "authenticate");
        final String token = Base64.encodeToString((username + ":" + password).getBytes(), Base64.DEFAULT);
        connection.setRequestProperty("Authorization", "Basic " + token);
        connection.setUseCaches(false);
        connection.connect();
        try {
            final int status = connection.getResponseCode();
            if (status == HttpURLConnection.HTTP_OK)
                return getSecurityToken();
            else {
                setSecurityToken(null);
                throw HttpException.build(connection);
            }
        } finally {
            connection.disconnect();
        }
    }

    /*
    static class CookieStoreWrapper implements CookieStore {

        final String TAG = getClass().getSimpleName();

        final CookieStore mStore;

        CookieStoreWrapper(CookieStore store) {
            mStore = store;
        }

        @Override
        public void add(URI uri, HttpCookie cookie) {
            mStore.add(uri, cookie);
            Log.i(TAG, "add " + uri + " = " + cookie);
        }

        @Override
        public List<HttpCookie> get(URI uri) {
            final List<HttpCookie> list = mStore.get(uri);
            Log.i(TAG, "get " + uri + " = " + list);
            return list;
        }

        @Override
        public List<HttpCookie> getCookies() {
            return mStore.getCookies();
        }

        @Override
        public List<URI> getURIs() {
            return mStore.getURIs();
        }

        @Override
        public boolean remove(URI uri, HttpCookie cookie) {
            final boolean result = mStore.remove(uri, cookie);
            if (result)
                Log.i(TAG, "del " + uri + " = " + cookie);
            return result;
        }

        @Override
        public boolean removeAll() {
            return mStore.removeAll();
        }

    }
    */

}
