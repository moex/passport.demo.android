package com.moex.passport.api;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class Resource extends Endpoint {

    public final static String PASSPORT_GRANTS_NAME = "X-MicexPassport-Marker";

    public final static String PASSPORT_GRANTED_VALUE = "granted";

    public Resource(final URL endpoint, String userAgent) {
        super(endpoint, userAgent);
    }

    public Response request(String resource) throws IOException {
        final HttpURLConnection connection = open("GET", resource);
        connection.connect();
        try {
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK)
                return new Response(connection);
            else
                throw HttpException.build(connection);
        } finally {
            connection.disconnect();
        }
    }

    public static class Response {

        public final boolean granted;

        public final String content;

        Response(HttpURLConnection connection) throws IOException {
            content = HttpUtils.load(connection);
            granted = HttpUtils.hasHeaderValue(connection, PASSPORT_GRANTS_NAME, PASSPORT_GRANTED_VALUE);
        }

    }

}
