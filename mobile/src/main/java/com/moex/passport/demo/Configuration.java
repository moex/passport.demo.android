package com.moex.passport.demo;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

/**
 * Stores some values in {@linkplain android.content.SharedPreferences}, last user name
 * and MOEX passport certificate (security token) among them. If application is finished
 * and started later this helps to load and use previous authentication token (if not
 * has expired yet).
 */
class Configuration {

    private String mLoginName;
    private String mSecurityToken;
    private boolean mIsDirty;

    public String getLoginName() {
        return mLoginName;
    }

    public Configuration setLoginName(String value) {
        if (TextUtils.isEmpty(value)) {
            mIsDirty = mLoginName != null;
            mLoginName = null;
            return this;
        }
        if (value.equals(mLoginName)) return this;
        mLoginName = value;
        mIsDirty = true;
        return this;
    }

    public String getSecurityToken() {
        return mSecurityToken;
    }

    public Configuration setSecurityToken(String value) {
        if (TextUtils.isEmpty(value)) {
            mIsDirty = mSecurityToken != null;
            mSecurityToken = null;
            return this;
        }
        if (value.equals(mSecurityToken)) return this;
        mSecurityToken = value;
        mIsDirty = true;
        return this;
    }

    public void load(Context context) {
        final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        mLoginName = sp.getString("login.name", null);
        mSecurityToken = sp.getString("web.cookie", null);
    }

    public void save(Context context) {
        if (TextUtils.isEmpty(mLoginName)) return;
        if (mIsDirty) {
            mIsDirty = false;
            PreferenceManager.getDefaultSharedPreferences(context)
                    .edit()
                    .putString("login.name", mLoginName)
                    .putString("web.cookie", mSecurityToken)
                    .commit();
        }
    }

}
