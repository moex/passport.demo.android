package com.moex.passport.demo;

/**
 * Created by User on 20.01.2015.
 */
public class Consts {

    final static String PassportEndpoint = "https://passport.beta.moex.com/";

    final static String ResourceEndpoint = "http://beta.moex.com/iss/";

    private Consts() {}

}
