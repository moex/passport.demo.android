package com.moex.passport.demo;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.moex.passport.api.HttpUtils;
import com.moex.passport.api.Passport;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * A {@link android.support.v4.app.Fragment} hosting a {@link com.moex.passport.demo.LoginTask}.
 * Retains its instance thus survives orientation changes and keeps reference to an asynchronous
 * task if any has been started.
 */
public class LoginHolder extends Fragment implements LoginTask.Callback {

    final static String TAG = LoginHolder.class.getName();

    public static LoginHolder get(FragmentManager manager) {
        if (manager.findFragmentByTag(TAG) == null) {
            manager.beginTransaction()
                    .add(new LoginHolder(), TAG).commit();
            manager.executePendingTransactions();
        }
        return (LoginHolder) manager.findFragmentByTag(TAG);
    }

    public LoginHolder() {
        setRetainInstance(true);
    }

    private Passport mPassport;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (mPassport == null) {
            try {
                mPassport = new Passport(new URL(Consts.PassportEndpoint));
                mPassport.setUserAgent(HttpUtils.getDefaultUserAgent(getActivity()));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
    }

    private LoginTask mLoginTask;

    private Object mTaskReply;

    private boolean mHasResult;

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mLoginTask != null)
            mLoginTask.cancel(false);
    }

    private LoginTask.Callback mCallback;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof LoginTask.Callback)
            mCallback = (LoginTask.Callback) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    @Override
    public void onCancelLogin() {
        mLoginTask = null;
        mHasResult = false;
        mTaskReply = null;
        if (mCallback == null) return;
        mCallback.onCancelLogin();
    }

    @Override
    public void onFinishLogin(Object result) {
        mLoginTask = null;
        if (mCallback == null) {
            mHasResult = true;
            mTaskReply = result;
        } else
            mCallback.onFinishLogin(result);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState == null) return;
        if (mHasResult && mCallback != null) {
            // Activity is recreated after configuration change.
            // While this change was under its way
            // the task has already finished.
            mCallback.onFinishLogin(mTaskReply);
            mTaskReply = null;
            mHasResult = false;
        }
    }

    public boolean isTaskRunning() {
        return mLoginTask != null;
    }

    public void startLoginTask(String username, String password) {
        if (isTaskRunning())
            throw new IllegalStateException();
        mLoginTask = new LoginTask(mPassport, this, username, password);
        mLoginTask.execute((Void) null);
    }

}
