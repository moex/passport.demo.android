package com.moex.passport.demo;

import android.os.AsyncTask;

import com.moex.passport.api.Passport;

import java.io.IOException;

/**
 * An {@link android.os.AsyncTask} used for authenticating
 * username/password agains MOEX passport out of main UI thread.
 */
public class LoginTask extends AsyncTask<Void,Void,Object> {

    /**
     * A callback to be notified when {@linkplain com.moex.passport.demo.LoginTask}
     * is finished or cancelled.
     */
    public static interface Callback {

        /**
         * Called when login sequence is cancelled.
         */
        void onCancelLogin();

        /**
         * Called when login sequence finishes
         * either successfully or with an exception.
         *
         * @param result A String value of MOEX passport certificate (security token)
         *               in case of success. Exception instance is case of failure.
         */
        void onFinishLogin(Object result);

    }

    private final Passport mPassport;
    private final Callback mCallback;
    private final String mUsername;
    private final String mPassword;

    LoginTask(Passport passport, Callback callback, String username, String password) {
        mPassport = passport;
        mCallback = callback;
        mUsername = username;
        mPassword = password;
    }

    @Override
    protected Object doInBackground(Void... params) {
        try {
            return mPassport.authenticate(mUsername, mPassword);
        } catch (IOException e) {
            return e;
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        mCallback.onCancelLogin();
    }

    @Override
    protected void onPostExecute(Object result) {
        super.onPostExecute(result);
        mCallback.onFinishLogin(result);
    }

}
