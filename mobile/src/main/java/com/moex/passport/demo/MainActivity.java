package com.moex.passport.demo;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.moex.passport.api.HttpUtils;
import com.moex.passport.api.Passport;
import com.moex.passport.api.Resource;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Calendar;

import static com.moex.passport.api.HttpUtils.disableSSLCertificateChecking;

public class MainActivity extends BaseActivity implements Handler.Callback {

    static {
        // For demo purposes only!
        // https://passport.beta.moex.com server certificate is invalid.
        // Remove this when ready for production.
        disableSSLCertificateChecking();
    }

    final static String TAG = MainActivity.class.getSimpleName();

    final static int REQUEST_LOGIN = 1;

    final static int MESSAGE_FETCH = 1;

    final static long INTERVAL_FETCH = 10000;  // update every 10 seconds

    final Handler mHandler;

    public MainActivity() {
        mHandler = new Handler(this);
    }

    Passport mPassport;

    Resource mResource;

    TextView mTickerName;
    TextView mTickerLast;
    TextView mTickerTime;
    TextView mTickerWhen;

    TextView mLoginState;
    Button mLoginStart;

    TextView mErrorText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            final String agent = HttpUtils.getDefaultUserAgent(this);
            mPassport = new Passport(new URL(Consts.PassportEndpoint), agent)
                    .setSecurityToken(mConfiguration.getSecurityToken());

            mResource = new Resource(new URL(Consts.ResourceEndpoint), agent);
        } catch (MalformedURLException e) {
            Log.e(TAG, e.toString());
            finish();
        }

        setContentView(R.layout.activity_main);

        mTickerName = (TextView) findViewById(R.id.ticker_name);
        mTickerLast = (TextView) findViewById(R.id.ticker_last);
        mTickerTime = (TextView) findViewById(R.id.ticker_time);
        mTickerWhen = (TextView) findViewById(R.id.ticker_when);

        mLoginState = (TextView) findViewById(R.id.login_state);
        mLoginStart = (Button) findViewById(R.id.login_start);
        mLoginStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestLogin();
            }
        });

        mErrorText = (TextView) findViewById(R.id.error_text);

//        if (mConfiguration.isEmpty()) {
//            final Intent intent =  new Intent(this, LoginActivity.class);
//            startActivityForResult(intent, REQUEST_LOGIN);
//        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            requestLogin();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    final void requestLogin() {
        final Intent intent =  new Intent(this, LoginActivity.class);
        startActivityForResult(intent, REQUEST_LOGIN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_LOGIN:
                // Reload configuration
                mConfiguration.load(this);
//                final String token = mConfiguration.getSecurityToken();
//                if (resultCode == RESULT_CANCELED && TextUtils.isEmpty(token))
//                    finish();
                break;
        }
    }

    private boolean mIsStarted;

    private FetchTask mFetchTask;

    @Override
    protected void onStart() {
        super.onStart();
        mIsStarted = true;
        mHandler.sendEmptyMessage(MESSAGE_FETCH);
    }

    @Override
    protected void onStop() {
        mHandler.removeMessages(MESSAGE_FETCH);
        mIsStarted = false;
        if (mFetchTask != null) {
            mFetchTask.cancel(false);
            mFetchTask = null;
        }
        super.onStop();
    }

    @Override
    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case MESSAGE_FETCH:
                if (mIsStarted) {
                    if (mFetchTask == null) {
                        // TODO
                        // You may want to check network connection state
                        // to avoid scheduling unnecessary async task
                        // if no network connection is established.
                        mFetchTask = new FetchTask();
                        mFetchTask.execute();
                    }
                    mHandler.sendEmptyMessageDelayed(MESSAGE_FETCH, INTERVAL_FETCH);
                }
                return true;
        }
        return false;
    }

    final static String USD_RUB_RESOURCE = "engines/currency/markets/selt/boards/CETS/securities.json?securities=USD000UTSTOM&iss.meta=off&lang=ru";

    static class Result {

        public final boolean granted;
        public final JSONObject content;
        public final Exception problem;

        Result(boolean granted, JSONObject content, Exception problem) {
            this.granted = granted;
            this.content = content;
            this.problem = problem;
        }

    }

    /**
     * A stub value indicating JSONException failure.
     */
    final static JSONObject FAILURE = new JSONObject();

    class FetchTask extends AsyncTask<Void,Void,Result> {

        @Override
        protected Result doInBackground(Void... params) {
            try {
                Log.d(TAG, "Loading " + USD_RUB_RESOURCE);
                final Resource.Response response = mResource.request(USD_RUB_RESOURCE);
                try {
                    final JSONObject content = new JSONObject(response.content);
                    return new Result(response.granted, content, null);
                } catch (JSONException e) {
                    Log.e(TAG, e.toString());
                    return new Result(response.granted, FAILURE, e);
                }
            } catch (IOException e) {
                Log.e(TAG, e.toString());
                return new Result(false, null, e);
            }
        }

        @Override
        protected void onPostExecute(Result result) {
            super.onPostExecute(result);

            if (isCancelled() || mFetchTask == null) return;
            mFetchTask = null;

            if (result.content == null) {
                // Got an IOException.
                // It is an error situation: resource is either invalid or
                // unavailable by some reason (server failure, network failure etc.)
            } else {
                // Got at least a "200 OK" reply.
                // Save (or remove if not authenticated and thus no grants by passport)
                // security token in configuration (for orientation changes and/or
                // temporary application shutdown).
                final String token = result.granted ? mPassport.getSecurityToken() : null;
                mConfiguration.setSecurityToken(token);
            }

            updateErrorUI(result.problem);
            updateStateUI(result.granted);
            updateDataUI(result.content);
        }

    }

    final void updateStateUI(boolean granted) {
        mLoginState.setText(granted ? R.string.authenticated : R.string.not_authenticated);
        mLoginStart.setVisibility(granted ? View.INVISIBLE : View.VISIBLE);
    }

    final void updateErrorUI(final Exception problem) {
        if (problem == null) {
            mErrorText.setVisibility(View.GONE);
        } else {
            final StringBuilder sb = new StringBuilder()
                    .append(DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime()))
                    .append("\r\n").append(problem.toString());
            mErrorText.setText(sb);
            mErrorText.setVisibility(View.VISIBLE);
        }
    }

    private int[] getIndices(JSONObject source, String[] names) {
        final int[] indices = new int[names.length];
        Arrays.fill(indices, -1);
        if (source == null) return indices;

        final JSONArray columns = source.optJSONArray("columns");
        if (columns == null) return indices;

        for (int i = 0; i < columns.length(); i++) {
            final String column = columns.optString(i);
            if (column == null) continue;
            for (int j = 0; j < names.length; j++)
                if (column.equalsIgnoreCase(names[j])) {
                    indices[j] = i;
                    break;
                }
        }
        return indices;
    }

    private Object[] getValues(final JSONArray source, int offset, int[] indices) {
        final Object[] values = new Object[indices.length];
        Arrays.fill(values, null);
        if (source == null || source.length() < 1) return values;
        final JSONArray record = source.optJSONArray(offset);
        if (record == null) return values;
        for (int i = 0; i < indices.length; i++) {
            final int index = indices[i];
            if (index >= 0 && index < record.length())
                values[i] = record.opt(index);
        }
        return values;
    }

    final static String[] TickerColumns = new String[] {
            "SECNAME", "DECIMALS"
    };

    final static String[] MarketColumns = new String[] {
            "LAST", "TIME"
    };

    static String asString(Object object) {
        return object == null ? null : object.toString();
    }

    static int asInteger(Object object) {
        if (object == null || object.equals(JSONObject.NULL)) return 0;
        if (object instanceof Number)
            return ((Number) object).intValue();
        try {
            return Integer.parseInt(object.toString());
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    static double asDouble(Object object) {
        if (object == null || object.equals(JSONObject.NULL)) return 0;
        if (object instanceof Number)
            return ((Number) object).doubleValue();
        try {
            return Double.parseDouble(object.toString());
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    private NumberFormat mPricer;

    final void updateDataUI(JSONObject content) {
        if (content == null || content == FAILURE) return;

        mTickerWhen.setText(DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime()));

        final JSONObject securities = content.optJSONObject("securities");
        if (securities != null) {
            final int[] tp = getIndices(securities, TickerColumns);
            final Object[] ts = getValues(securities.optJSONArray("data"), 0, tp);
            mTickerName.setText(asString(ts[0]));

            final int decimals = asInteger(ts[1]);
            if (mPricer == null || mPricer.getMaximumFractionDigits() != decimals) {
                mPricer = NumberFormat.getNumberInstance();
                mPricer.setMinimumFractionDigits(decimals);
                mPricer.setMaximumFractionDigits(decimals);
            }
        }

        final JSONObject marketdata = content.optJSONObject("marketdata");
        if (marketdata != null) {
            final int[] mp = getIndices(marketdata, MarketColumns);
            final Object[] ms = getValues(marketdata.optJSONArray("data"), 0, mp);
            mTickerLast.setText(mPricer == null
                    ? asString(ms[0]) : mPricer.format(asDouble(ms[0])));
            mTickerTime.setText(asString(ms[1]));
        }
    }

}
